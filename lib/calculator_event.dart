import 'package:equatable/equatable.dart';

abstract class CalculatorState extends Equatable {}

class InitState extends CalculatorState {
  @override
  List<Object> get props => [];
}

class CalculationDoneState extends CalculatorState {
  CalculationDoneState(this.result);

  final int result;

  @override
  List<Object> get props => [result];

  @override
  String toString() {
    return result.toString();
  }
}

class CalculationErrorState extends CalculatorState {
  CalculationErrorState(this.errorMsg);

  final String errorMsg;

  @override
  List<Object> get props => [errorMsg];
}