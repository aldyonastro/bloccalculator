import 'package:bloc_calculator/calculator_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'calculator_event.dart';
import 'calculator_observer.dart';

class CalculatorPage extends StatefulWidget {
  CalculatorPage({Key key}) : super(key: key);

  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {

  final _text1Controller = TextEditingController();
  final _text2Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    Bloc.observer = CalculatorObserver();
  }

  @override
  Widget build(BuildContext context) {
    int res = 0;
    String errorMessage = "";

    return Scaffold(
      appBar: AppBar(title: const Text('Counter')),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: BlocBuilder<CalculatorCubit, CalculatorState>(
          builder: (context, state) {

            if(state is CalculationDoneState){
              res = state.result;
              errorMessage = "";
            }

            if(state is CalculationErrorState){
              errorMessage = state.errorMsg;
            }

            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      flex: 4,
                      child: TextField(
                        controller: _text1Controller,
                        onChanged: (text) {
                          print("First text field: $text");
                        },
                      ),
                    ),
                    Spacer(flex: 1,),
                    Expanded(
                      flex: 4,
                      child: TextField(
                        controller: _text2Controller,
                        onChanged: (text) {
                          print("Second text field: $text");
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 32,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      flex: 7,
                      child: RaisedButton(
                        onPressed: () => context.bloc<CalculatorCubit>().procesPlusTotal(_text1Controller.text, _text2Controller.text),
                        child: Text('+', style: Theme.of(context).textTheme.headline6,),
                      ),
                    ),
                    Spacer(flex: 1,),
                    Expanded(
                      flex: 7,
                      child: RaisedButton(
                        onPressed: () => context.bloc<CalculatorCubit>().procesMinTotal(_text1Controller.text, _text2Controller.text),
                        child: Text('-', style: Theme.of(context).textTheme.headline6,),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 32,),
                FittedBox(
                  child: Text(errorMessage == "" ?
                    "Result : "+res.toString() :
                    errorMessage,
                    style: Theme.of(context).textTheme.headline3,),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}