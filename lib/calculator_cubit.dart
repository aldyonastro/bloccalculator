import 'package:flutter_bloc/flutter_bloc.dart';
import 'calculator_event.dart';

//class CalculatorCubit extends Cubit<int> {
class CalculatorCubit extends Cubit<CalculatorState> {

  CalculatorCubit() : super(InitState());

  void procesPlusTotal(String x, String y) {
    try{
      int num1 = int.parse(x);
      int num2 = int.parse(y);
//      emit(num1 + num2);
      emit(CalculationDoneState(num1 + num2));
    }
    catch(e){
      emit(CalculationErrorState(e.toString()));
    }
  }

  void procesMinTotal(String x, String y) {
    try{
      int num1 = int.parse(x);
      int num2 = int.parse(y);
//      emit(num1 - num2);
      emit(CalculationDoneState(num1 - num2));
    }
    catch(e){
      emit(CalculationErrorState(e.toString()));
    }

  }
}