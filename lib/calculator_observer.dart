import 'package:bloc/bloc.dart';

class CalculatorObserver extends BlocObserver {
  @override
  void onChange(Cubit cubit, Change change) {
    print('${cubit.runtimeType.toString()} $change');
    super.onChange(cubit, change);
  }
}